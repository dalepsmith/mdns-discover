function saveOptions(e) {
    e.preventDefault();
    browser.storage.local.set({
        address: document.querySelector("#address").checked,
        type: document.querySelector("#type").value
    });
}

function restoreOptions() {
    function setCurrentChoice(result) {
        document.querySelector("#address").checked = result.address || false;
        document.querySelector("#type").value = result.type || "_http._tcp";
    }

    function onError(error) {
        console.log(`Error: ${error}`);
    }

    browser.storage.local.get().then(setCurrentChoice, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
