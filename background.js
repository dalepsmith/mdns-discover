var devices = {};
var notify = false;
var address = false;
var service_type = '_http._tcp';

var mdns = browser.runtime.connectNative("mdns.discover");

browser.browserAction.setBadgeText({text: '0'});
browser.browserAction.setBadgeBackgroundColor({color: "green"});

// Defaults are white on red

const titles = {
    'add': 'Added',
    'remove': 'Removed'
};

function notification(type, service) {
    if (!notify)
        return;

    browser.notifications.create({
        type: 'basic',
        title: titles[type],
        message: service
    });
}

function mdns_message_listener(resp) {
    switch (resp.type) {
    case 'add':
        devices[resp.service] = resp;
        break;
    case 'remove':
        delete devices[resp.service];
        break;
    }
    notification(resp.type, resp.service);
    browser.browserAction.setBadgeText({text: Object.keys(devices).length.toString()});
    browser.runtime.sendMessage(resp).then(() => {}, () => {});
}

function mdns_disconnect_listener(p) {
    if (p.error) {
        // 'No such native application mdns.discover'

        devices['1'] = { type:'error', service:'1', message:p.error.message };
        devices['2'] = { type:'error', service:'2', message:'Please install the native application' };
        devices['3'] = { type:'error', service:'3', message: 'See avahi-mdns-discover (for Linux)',
                         url: 'https://bitbucket.org/dalepsmith/avahi-mdns-discover/' };
        devices['4'] = { type:'error', service:'4', message: 'Or bonjour-mdns-discover (bsd, mac)',
                         url: 'https://bitbucket.org/dalepsmith/bonjour-mdns-discover/' };
        devices['5'] = { type:'error', service:'5', message: 'Or even rust-mdns-discover (for Windows)',
                         url: 'https://bitbucket.org/dalepsmith/rust-mdns-discover/downloads/' };

        browser.browserAction.setBadgeText({text: 'X'});
        browser.browserAction.setBadgeBackgroundColor({color: "red"});
    }
}

mdns.onMessage.addListener(mdns_message_listener);
mdns.onDisconnect.addListener(mdns_disconnect_listener);

function repost_message(type, service) {
    devices = {};
    browser.browserAction.setBadgeText({text: '0'});
    mdns.postMessage(service_type);
}

browser.storage.local.get()
    .then((result) => {
        address = result.address || false;
        service_type = result.type || service_type;
        mdns.postMessage(service_type);
    }, (error) => { console.log(`Error: ${error}`); });

browser.storage.onChanged.addListener((changes, area) => {
    if (area != 'local')
        return;

    for (let item of Object.keys(changes)) {
        switch (item) {
        case 'address':
            address = changes[item].newValue;
            break;
        case 'type':
            if (changes[item].oldValue != changes[item].newValue) {
                service_type = changes[item].newValue;
                repost_message();
            }
            break;
        }
    }
});

browser.permissions.onAdded.addListener((perm) => {
    if (perm.permissions == 'notifications') {
        notify = true;
        repost_message();
    }
});

browser.permissions.onRemoved.addListener((perm) => {
    if (perm.permissions == 'notifications') {
        notify = false;
        repost_message();
    }
});

browser.permissions.getAll().then((result) => {
    for (let item of result.permissions)
        if (item == 'notifications')
            notify = true;
});
