
mDNS Discover watches for Multicast DNS service announcements and
collects them as links in a convenient browser action popup menu.

You can look for HTTP servers (service type `_http._tcp`) or for LXI
Instruments (service type `_lxi._tcp`).

You can optonially allow browser notifications to see service
add/remove messages.  Convenient when waiting for a server to reboot.

Hovering over a device in the menu shows a tooltip with device details
such as hostname, ip address, and URL. LXI devices also have the
Model, SerialNumber, FirmwareVersion, and Manufacturer.

Clicking on an item opens it in a new tab and switches to the tab.
Control Clicking does the same, but does not switch tabs.

mDNS Discover requires a companion native messaging application for
doing the multicast mDNS service discovery.  There are implementation
sources available for
[Avahi](https://bitbucket.org/dalepsmith/avahi-mdns-discover/),
[Bonjour](https://bitbucket.org/dalepsmith/avahi-mdns-discover/), and
[Windows (Rust native)](https://bitbucket.org/dalepsmith/rust-mdns-discover/downloads/).

The Rust native implementation also provides an (unsigned) .msi installer.

For more information on mDNS and service types see
[Multicast DNS](http://www.multicastdns.org/),
[RFC6762: Multicast DNS](https://datatracker.ietf.org/doc/html/rfc6762) and
[RFC6763: DNS-Based Service Discovery](https://datatracker.ietf.org/doc/html/rfc6763).
