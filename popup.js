// A response looks like either
// {
//     type: 'remove',
//     service: 'Service Name'
// }

// Or
// {
//     type: 'add',
//     srv: '_lxi._tcp' or '_http._tcp'
//     service: 'Service Name',
//     hostname: 'The hostname',
//     address: 'The IPv4 address',
//     port: 'optional Port number',
//     txt: {
//         'key1': 'Value one',
//         'key2': 'Value two',
//         ...
//         'keyN': 'Value N'
//     }
// }

// And if there is no native app running
// {
//     type: 'error',
//     service: '1',
//     message: 'An error message text'
// }

var address = false;

function format(fmt, env) {
    let out = "";
    for (let i = 0; i < fmt.length; i++) {
        if (fmt[i] != "%") {
            out += fmt[i];
            continue;
        }
        i++;
        if (i == fmt.length) {
            out += "%"; // '%' at EOS is '%'  ?
            break;
        }
        switch (fmt[i]) {
        default: out += fmt[i]; break;
        case "A": out += env.address ?? ""; break;
        case "H": out += (address) ? (env.address ?? "") : (env.hostname ?? ""); break;
        case "N": out += env.hostname ?? ""; break;
        case "P": out += env.port ?? ""; break;
        case "S": out += env.service ?? ""; break;
        case "U": out += env.url ?? ""; break;
        case "{":
            {
	        // See https://datatracker.ietf.org/doc/html/rfc6763#section-6.4
	        // %{key=default}
	        // Since '=' is the only char not allowed in a TXT key,
	        // we use that to delimit the default value.
	        // Assumes '}' is not used in a key.
	        i++;
	        let key = "";
	        let def = "";
	        while (i < fmt.length && fmt[i] != "=" && fmt[i] != "}") {
                    key += fmt[i++];
	        }
	        if (fmt[i] == "=") {
                    i++;
                    while (i < fmt.length && fmt[i] != "}") {
                        def += fmt[i++];
                    }
	        }
	        out += env.txt?.[key] ?? def;
            }
            break;
        }
    }
    return out;
}

function format_href(resp) {
    let fmt = '';
    let env = resp;

    switch (resp.srv) {
    case '_lxi._tcp':
        fmt = 'http://%H:%P/';
        break;
    case '_http._tcp':
        fmt = 'http://%H:%P%{path=/}';
        break;
    case '_https._tcp':
        fmt = 'https://%H:%P%{path=/}';
        break;
    }
    return format(fmt, env);
}

function format_tooltip(resp) {
    let fmt = '';
    let env = resp;
    env.url = format_href(resp);

    switch (resp.srv) {
    case '_lxi._tcp':
        return format(" Hostname: %N \n" +
                      " Address: %A \n" +
                      " Model: %{Model} \n" +
                      " SerialNumber: %{SerialNumber} \n" +
                      " FirmwareVersion: %{FirmwareVersion} \n" +
                      " Manufacturer: %{Manufacturer} \n" +
                      " URL: %U ",
                      env);
    case '_https._tcp':
    case '_http._tcp':
        return format(" Hostname: %N \n" +
                      " Address: %A \n" +
                      " URL: %U ",
                      env);
    }
}

function add_device(resp) {
    var item = document.createElement("div");
    item.setAttribute('id', resp.service);
    item.setAttribute('url', format_href(resp));
    item.setAttribute('title', format_tooltip(resp));
    item.setAttribute('class', "panel-list-item");
    item.addEventListener('click', function clicker(event) {
        browser.tabs.create({url: this.attributes.url.value, active: !event.ctrlKey});
    }, false);
    item.appendChild(document.createTextNode(resp.service));

    const the_list = document.getElementById('the-list');
    // Keeps items in sorted order.
    for (var i in the_list.childNodes) {
        // If the new item is less than the current node, insert it
        // and return.
        if (resp.service < the_list.childNodes[i].id) {
            the_list.insertBefore(item, the_list.childNodes[i]);
            return;
        }
    }
    // Not less than any, so append it.
    the_list.appendChild(item);
}

function del_device(resp) {
    document.getElementById('the-list').removeChild(document.getElementById(resp.service));
}

function error_message(resp) {
    const the_list = document.getElementById('the-list');
    var item = document.createElement("div");
    item.setAttribute('class', "panel-list-item");
    if (resp.url) {
        item.setAttribute('url', resp.url);
        item.setAttribute('title', resp.url);
        item.addEventListener('click', function clicker(event) {
            browser.tabs.create({url: this.attributes.url.value, active: !event.ctrlKey});
        }, false);
    }
    item.appendChild(document.createTextNode(resp.message));
    the_list.appendChild(item);
}

function handler(resp) {
    switch (resp.type) {
    case 'add':
        add_device(resp);
        break;
    case 'remove':
        del_device(resp);
        break;
    case 'error':
        error_message(resp);
        break;
    }
}

document.addEventListener('DOMContentLoaded', () => {
    // Get all previously seen devices
    browser.runtime.getBackgroundPage((bg) => {
        address = bg.address;
        for (var key in bg.devices)
            if (bg.devices.hasOwnProperty(key))
                handler(bg.devices[key]);
    });

    // Listen for add/remove events
    browser.runtime.onMessage.addListener(handler);
});
